package com.oaker.hours.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.oaker.hours.doman.entity.MhArchive;
import com.oaker.hours.doman.entity.MhConfig;
import com.oaker.hours.doman.vo.MhArchiveDetailVO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MhArchiveMapper extends BaseMapper<MhArchive> {

    MhArchive query(Long archiveId);
}
