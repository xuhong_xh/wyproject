package com.oaker.hours.doman.dto;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
public class MhArchiveDTO {
    @ApiModelProperty(value = "部门ID")
    private Long deptId;


    @ApiModelProperty(value = "归档时间")
    private String archiveDate;
}
