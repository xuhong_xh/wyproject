package com.oaker.hours.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.oaker.hours.doman.entity.MhArchiveHour;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MhArchiveHourMapper   {

    void insertBatch(List<MhArchiveHour> list);

    List<MhArchiveHour> queryByArchiveId(Long archiveId);
}
