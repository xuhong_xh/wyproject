package com.oaker.hours.doman.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;


@Data
@ApiModel(value = "工时相关参数修改")
public class SysAppConfigDTO {
    /** 每日工时数 */
    private BigDecimal workTime;

    /** 每月工作日数 */
    private int workDay;
    /** 是否允许加班 */
    private boolean overtimeAllow;

    /** 是否开启审核 */
    private boolean workReview;

    /** 是否记录0工时项目 */
    private boolean hourZero;

    private boolean archive;
}
