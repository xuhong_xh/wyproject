package com.oaker.hours.doman.vo;


import com.baomidou.mybatisplus.annotations.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel(value = "MhArchiveVO", description = "归档查询列表返回类")
public class MhArchiveVO {


    @ApiModelProperty("id")
    private Long Id;

    @ApiModelProperty("部门ID")
    private Long deptId;

    @ApiModelProperty("月份")
    private String archiveDate;

    @ApiModelProperty("填报状态")
    private boolean status;

    /** 创建时间 */
    @TableField(value = "create_time")
    private Date createTime;

    /** 创建人 */
    @TableField(value = "create_user")
    private int createUser;
}
