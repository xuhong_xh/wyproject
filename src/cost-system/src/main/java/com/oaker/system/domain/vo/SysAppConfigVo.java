package com.oaker.system.domain.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Description : 应用配置类
 * <功能详细描述>
 * @author: 须尽欢_____
 * @Data : 2021/11/4 10:34
 */
@Data
public class SysAppConfigVo {



    /** 每日工时数 */
    private BigDecimal workTime;

    /** 每月工作日数 */
    private int workDay;
    /** 是否允许加班 */
    private boolean overtimeAllow;

    /** 是否开启审核 */
    private boolean workReview;

    /** 是否记录0工时项目 */
    private boolean hourZero;

    private boolean mhArchive;


    public boolean isOvertimeAllow() {
        return overtimeAllow;
    }

    public boolean isWorkReview() {
        return workReview;
    }

    public boolean isHourZero() {
        return hourZero;
    }

    public boolean isArchive(){return mhArchive;}

}
